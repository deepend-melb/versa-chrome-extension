
function parseData (json) {
  let response = JSON.parse(json);
  let responseList = response.results;
  var page, pageValue, start, end, table;
  var sites = [];

  for (page of responseList){
    //Create html table 
    pageValue = page.body.storage.value;
    start = pageValue.search("<table");
    end = pageValue.search("</table") + 8;
    table = document.createElement('table');
    table.innerHTML = pageValue.substring(start, end);
    if (table.querySelector("tbody")) {
      var sitesList = formatObj(table);
      for (var site of sitesList) {
        sites.push(site);
      }
    }
  }
  return sites
}

function formatObj(table){
  var sites = [];
  var rows = getSiteRows(table);
  for (var i = 1; i < rows.length; i++){
    var site = {
      title: '',
       links: {
      }
    };
    var linklist = siteLinks(rows[i]);
    if (linklist.length >= 1) {
      if (linklist[0] !== undefined) {
        site.links.dev = linklist[0];
      }
  
      if (linklist[1] !== undefined) {
        site.links.uat = linklist[1];
      }
      
      if (linklist[2] !== undefined) {
        site.links.prod = linklist[2];
      }
  
      site.title = siteTitle(rows[i]);
      sites.push(site)
    }
  }
  return sites;
}
function siteLinks(tableRow){
  linkElements = tableRow.querySelectorAll('a');
  var links = [];
  for(linkElement of linkElements){
    links.push(linkElement.href);
  }
  return links;
}

function siteTitle(item){
  var rows = item.querySelectorAll('td')
  return rows[0].textContent;
}

function getSiteRows(table){
  var tableRows = table.querySelectorAll('tr');
  return tableRows;
}

function getSiteColumns(tableRow){
    var tableColumns = tableRow.querySelectorAll('td');
    return tableColumns;
}

function getData () {
  var xhr = new XMLHttpRequest()
  xhr.open('GET', 'https://versa.atlassian.net/wiki/rest/api/content?title=Environments&expand=body.storage', true)
  xhr.withCredentials = true;
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send()
  xhr.onload = function() {
    var json = xhr.responseText;
    chrome.storage.sync.set({link_list: parseData(json)}, function() {
    });
  }
}

chrome.runtime.onInstalled.addListener(function() {
  getData()
});


