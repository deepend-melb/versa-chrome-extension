chrome.storage.sync.get('link_list', function(data) {
  buildList(data)
});

function buildList (data) {
  let mylist = document.getElementById('list'); 
  let siteArray = data.link_list;
  mylist.innerHTML = ''
  
  for (var site of siteArray) {
    // Create button and panel for accordion menu
    let mySiteButton = document.createElement('button');
    let mySiteLabel = document.createElement('span');
    mySiteButton.className = 'accordion';
    let arrowDown = document.createElement('i');
    arrowDown.className = 'arrow-down';
    let mySitePanel = document.createElement('div');
    mySitePanel.className = 'panel';
    
    //Create li-element in sitelist, create url-list for site's links
    let mySiteWrapper = document.createElement('li'),
        myUrlsListWrapper = document.createElement('ul'),
        mySiteElementWrapper = document.createElement('li');

    myUrlsListWrapper.className = 'urlList';
    
    //Loop through links 
    for (var url in site.links) {
      let myElementLinkWrapper = document.createElement('li');
      
      if (site.links.hasOwnProperty(url)) {
        let myLinkWrapper = document.createElement('a');
        myLinkWrapper.className = 'aLink';
        myLinkWrapper.innerHTML = url;
        myLinkWrapper.href = site.links[url];
        myLinkWrapper.target = "_blank";
        myElementLinkWrapper.appendChild(myLinkWrapper)
      }
      //Add url to url-list
      myUrlsListWrapper.appendChild(myElementLinkWrapper);
    }
    
    //Set title of button
    mySiteLabel.innerHTML = site.title
    mySiteButton.appendChild(mySiteLabel);
    mySiteButton.appendChild(arrowDown);

    //Add site to panel
    mySitePanel.appendChild(myUrlsListWrapper);
    
    //Add button and panel to site-list
    mySiteElementWrapper.appendChild(mySiteButton);
    mySiteElementWrapper.appendChild(mySitePanel);
    mySiteElementWrapper.className = 'list__item'
    mylist.appendChild(mySiteElementWrapper);

    /* Accordion buttons */
    mySiteButton.addEventListener('click', function() {
      this.classList.toggle('active');
      arrowDown.classList.toggle('active');

      var panel = this.nextElementSibling;
      if (panel.style.display === 'block') {
        panel.style.display = 'none';
      } else {
        panel.style.display = 'block';
      }
    })
  }
}

function getData () {
  var xhr = new XMLHttpRequest()
  xhr.open('GET', 'https://versa.atlassian.net/wiki/rest/api/content?title=Environments&expand=body.storage', true)
  xhr.withCredentials = true;
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send()
  xhr.onload = function() {
    var json = xhr.responseText;
    var data = {
      link_list: parseData(json)
    }
    buildList(data)
  }
}

function parseData (json) {
  let response = JSON.parse(json);
  let responseList = response.results;
  var page, pageValue, start, end, table;
  var sites = [];

  for (page of responseList){
    //Create html table 
    pageValue = page.body.storage.value;
    start = pageValue.search("<table");
    end = pageValue.search("</table") + 8;
    table = document.createElement('table');
    table.innerHTML = pageValue.substring(start, end);
    if (table.querySelector("tbody")) {
      var sitesList = formatObj(table);
      for (var site of sitesList) {
        sites.push(site);
      }
    }
  }
  return sites
}

function formatObj(table){
  var sites = [];
  var rows = getSiteRows(table);
  for (var i = 1; i < rows.length; i++){
    var site = {
      title: '',
       links: {
      }
    };
    var linklist = siteLinks(rows[i]);
    if (linklist.length >= 1) {
      if (linklist[0] !== undefined) {
        site.links.dev = linklist[0];
      }
  
      if (linklist[1] !== undefined) {
        site.links.uat = linklist[1];
      }
      
      if (linklist[2] !== undefined) {
        site.links.prod = linklist[2];
      }
  
      site.title = siteTitle(rows[i]);

      sites.push(site)
    }
  }
  return sites;
}
function siteLinks(tableRow){
  linkElements = tableRow.querySelectorAll('a');
  var links = [];
  for(linkElement of linkElements){
    links.push(linkElement.href);
  }
  return links;
}

function siteTitle(item){
  var rows = item.querySelectorAll('td')
  return rows[0].textContent;
}

function getSiteRows(table){
  var tableRows = table.querySelectorAll('tr');
  return tableRows;
}

function getSiteColumns(tableRow){
    var tableColumns = tableRow.querySelectorAll('td');
    return tableColumns;
}


document.getElementById('refresh').addEventListener('click', function(e) {
  getData();
})

// Filter function, not completed
document.getElementById('search').addEventListener('input', function(e) {
  var sites = document.getElementsByClassName('accordion');
  for (let i = 0; i < sites.length; i++){
    sites[i].classList.toggle('active');
    if (sites[i].innerText.includes((e.target.value).toUpperCase())){
      sites[i].parentNode.style.display = 'block';
    }
    else {
      sites[i].parentNode.style.display = 'none';
    }
  }
})


chrome.runtime.onInstalled.addListener(function() {
  getData();
});